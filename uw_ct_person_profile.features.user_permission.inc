<?php

/**
 * @file
 * uw_ct_person_profile.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_ct_person_profile_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create uw_ct_person_profile content'.
  $permissions['create uw_ct_person_profile content'] = array(
    'name' => 'create uw_ct_person_profile content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any uw_ct_person_profile content'.
  $permissions['delete any uw_ct_person_profile content'] = array(
    'name' => 'delete any uw_ct_person_profile content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own uw_ct_person_profile content'.
  $permissions['delete own uw_ct_person_profile content'] = array(
    'name' => 'delete own uw_ct_person_profile content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any uw_ct_person_profile content'.
  $permissions['edit any uw_ct_person_profile content'] = array(
    'name' => 'edit any uw_ct_person_profile content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own uw_ct_person_profile content'.
  $permissions['edit own uw_ct_person_profile content'] = array(
    'name' => 'edit own uw_ct_person_profile content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'enter uw_ct_person_profile revision log entry'.
  $permissions['enter uw_ct_person_profile revision log entry'] = array(
    'name' => 'enter uw_ct_person_profile revision log entry',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_ct_person_profile authored by option'.
  $permissions['override uw_ct_person_profile authored by option'] = array(
    'name' => 'override uw_ct_person_profile authored by option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_ct_person_profile authored on option'.
  $permissions['override uw_ct_person_profile authored on option'] = array(
    'name' => 'override uw_ct_person_profile authored on option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_ct_person_profile promote to front page option'.
  $permissions['override uw_ct_person_profile promote to front page option'] = array(
    'name' => 'override uw_ct_person_profile promote to front page option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_ct_person_profile published option'.
  $permissions['override uw_ct_person_profile published option'] = array(
    'name' => 'override uw_ct_person_profile published option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_ct_person_profile revision option'.
  $permissions['override uw_ct_person_profile revision option'] = array(
    'name' => 'override uw_ct_person_profile revision option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_ct_person_profile sticky option'.
  $permissions['override uw_ct_person_profile sticky option'] = array(
    'name' => 'override uw_ct_person_profile sticky option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  return $permissions;
}

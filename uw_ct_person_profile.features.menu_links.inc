<?php

/**
 * @file
 * uw_ct_person_profile.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function uw_ct_person_profile_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-site-manager-vocabularies_profile-type:admin/structure/taxonomy/uwaterloo_profiles.
  $menu_links['menu-site-manager-vocabularies_profile-type:admin/structure/taxonomy/uwaterloo_profiles'] = array(
    'menu_name' => 'menu-site-manager-vocabularies',
    'link_path' => 'admin/structure/taxonomy/uwaterloo_profiles',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => 'Profile type',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-manager-vocabularies_profile-type:admin/structure/taxonomy/uwaterloo_profiles',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'language' => 'und',
    'menu_links_customized' => 0,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Profile type');

  return $menu_links;
}

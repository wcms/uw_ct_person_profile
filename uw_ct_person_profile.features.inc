<?php

/**
 * @file
 * uw_ct_person_profile.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_ct_person_profile_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function uw_ct_person_profile_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function uw_ct_person_profile_image_default_styles() {
  $styles = array();

  // Exported image style: person-profile-list.
  $styles['person-profile-list'] = array(
    'effects' => array(
      1 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 100,
          'height' => '',
          'upscale' => 1,
        ),
        'weight' => 1,
      ),
    ),
    'label' => 'person-profile-list',
  );

  // Exported image style: profile-photo-block.
  $styles['profile-photo-block'] = array(
    'effects' => array(
      1 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 220,
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
    'label' => 'profile-photo-block',
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function uw_ct_person_profile_node_info() {
  $items = array(
    'uw_ct_person_profile' => array(
      'name' => t('Person profile'),
      'base' => 'node_content',
      'description' => t('A person profile, which is optionally promoted to the front page sidebar and appears under the "Person Profiles" section of your site.'),
      'has_title' => '1',
      'title_label' => t('Name'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}

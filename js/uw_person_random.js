/**
 * @file
 */

// Shim to make sure Date.now works in all browsers.
if (!Date.now) {
  Date.now = function () { return new Date().getTime(); }
}

(function ($) {
  $(function () {
    // Load a random person profile, seeded with the current time so caching doesn't serve everyone the same person profile.
    $('#random-person').load(Drupal.settings.basePath + 'ajax/person/?uwb=rand&now=' + Date.now(), function () {
      // We don't need to check if the page didn't load, because the original person profile will still be there if it didn't.
      $('#random-person').show();

    });
  });

})(jQuery);

<?php

/**
 * @file
 * uw_ct_person_profile.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function uw_ct_person_profile_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'people_profiles_categories';
  $context->description = 'Displays people profiles categories (taxonomy) blocks.';
  $context->tag = 'Content';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'people-profiles' => 'people-profiles',
        'people-profiles/*' => 'people-profiles/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'uw_ct_person_profile-profile_by_type' => array(
          'module' => 'uw_ct_person_profile',
          'delta' => 'profile_by_type',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Content');
  t('Displays people profiles categories (taxonomy) blocks.');
  $export['people_profiles_categories'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'person_profile-front_page';
  $context->description = 'Displays a random person profile (promoted to front page) in the sidebar.';
  $context->tag = 'Content';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '<front>' => '<front>',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'uw_ct_person_profile-person' => array(
          'module' => 'uw_ct_person_profile',
          'delta' => 'person',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Content');
  t('Displays a random person profile (promoted to front page) in the sidebar.');
  $export['person_profile-front_page'] = $context;

  return $export;
}
